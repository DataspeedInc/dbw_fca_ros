^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Changelog for package dbw_fca_joystick_demo
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1.3.3 (2024-01-02)
------------------

1.3.2 (2023-09-11)
------------------

1.3.1 (2023-05-10)
------------------

1.3.0 (2022-11-30)
------------------

1.2.3 (2022-05-13)
------------------

1.2.2 (2022-03-08)
------------------

1.2.1 (2021-09-20)
------------------

1.2.0 (2021-05-12)
------------------

1.1.3 (2021-03-09)
------------------
* Add option to use socketcan hardware
* Contributors: Kevin Hallenbeck

1.1.2 (2021-01-14)
------------------

1.1.1 (2020-08-17)
------------------

1.1.0 (2020-08-10)
------------------

1.0.11 (2020-08-05)
-------------------

1.0.10 (2020-07-09)
-------------------
* Increase CMake minimum version to 3.0.2 to avoid warning about CMP0048
  http://wiki.ros.org/noetic/Migration#Increase_required_CMake_version_to_avoid_author_warning
* Contributors: Kevin Hallenbeck

1.0.9 (2020-02-14)
------------------
* Add door commands
* Contributors: Kevin Hallenbeck

1.0.8 (2019-10-17)
------------------

1.0.7 (2019-09-13)
------------------

1.0.6 (2019-08-13)
------------------

1.0.5 (2019-07-24)
------------------

1.0.4 (2019-07-11)
------------------

1.0.3 (2019-05-03)
------------------

1.0.2 (2019-03-14)
------------------

1.0.1 (2019-03-01)
------------------

1.0.0 (2018-11-30)
------------------
* Use the ${catkin_EXPORTED_TARGETS} macro for target dependencies
* Removed joystick deadzone
* Added parameters for brake and throttle gains (sanitized from 0 to 1)
* Contributors: Kevin Hallenbeck

0.0.2 (2018-10-23)
------------------
* Added option to command steering torque instead of position
* Added option to enable/disable each command topic
* Contributors: Kevin Hallenbeck

0.0.1 (2018-08-08)
------------------
* Initial release
* Contributors: Kevin Hallenbeck
