#ifndef DBW_FCA_MSGS_MESSAGE_TURNSIGNALCMD_H
#define DBW_FCA_MSGS_MESSAGE_TURNSIGNALCMD_H


#include <dbw_fca_msgs/MiscCmd.h>

namespace dbw_fca_msgs
{

// Backwards compatibility with old message type
typedef MiscCmd TurnSignalCmd;

} // namespace dbw_fca_msgs

#endif // DBW_FCA_MSGS_MESSAGE_TURNSIGNALCMD_H
